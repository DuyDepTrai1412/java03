package Buoi2;

import java.util.Scanner;

public class Bai1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập số phần tử: ");
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            System.out.print("array[" + i + "] = ");
            array[i] = sc.nextInt();
        }
        int choose;
        System.out.println("1, Sắp xếp tăng dần");
        System.out.println("2, Sắp xếp giảm dần");
        choose = sc.nextInt();
        switch(choose){
            case 1:
                for (int i = 0; i < array.length-1; i++) {
                    for (int j = i+1; j < array.length; j++) {
                        if(array[i] > array[j]){
                            int temp = array[i];
                            array[i] = array[j];
                            array[j] = temp;
                        }
                    }
                }
                for (int i = 0; i < array.length; i++) {
                    System.out.print(array[i] + " ");

                }
            case 2:
                for (int i = 0; i < array.length-1; i++) {
                    for (int j = i+1; j < array.length; j++) {
                        if(array[i] < array[j]){
                            int temp = array[i];
                            array[i] = array[j];
                            array[j] = temp;
                        }
                    }
                }
                for (int i = 0; i < array.length; i++) {
                    System.out.print(array[i] + " ");

                }
        }
    }
}
