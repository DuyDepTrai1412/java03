package Buoi2;


import java.util.Scanner;

public class Bai15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int choose;
        String string;
        System.out.println("Nhập chuỗi: ");
        string = sc.nextLine();
        while(true) {
            System.out.print("\tChọn chức năng: \n 1, Viết hoa chuỗi.\n2, Viết thường chuỗi.\n3, Thoát chương trình.\n");
            choose = sc.nextInt();
            switch (choose) {
                case 1:
                    System.out.println(string.toUpperCase());
                    break;
                case 2:
                    System.out.println(string.toLowerCase());
                    break;
                case 3:
                    return;
                default:
                    System.out.println("Moi chon lai.");
                    break;
            }
        }
    }



}
