package Buoi2;

import java.util.Scanner;

public class Bai11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sum;
        System.out.println("nhập kích thước ma trận: ");
        int n = sc.nextInt();
        int[][] arrayA = new int[n][n];
        int[][] arrayB = new int[n][n];
        int[][] arrayC = new int[n][n];
        //  int[][] arrayB = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print("A[" + i + "][" + j + "] = ");
                arrayA[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print("B[" + i + "][" + j + "] = ");
                arrayB[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= n; j++) {
                sum = 0;
                for (int k = 0; k <= n; k++) {
                    sum += arrayA[i][k] * arrayB[k][j];
                }
                arrayC[i][j] = sum;
            }
        }
    }
}
